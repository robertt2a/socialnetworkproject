package com.rt.socialNetworkProject.controller;

import com.rt.socialNetworkProject.model.dto.ImageFileDto;
import com.rt.socialNetworkProject.model.entity.ImageFile;
import com.rt.socialNetworkProject.service.ImageFileStorageService;
import com.rt.socialNetworkProject.service.PostService;
import com.rt.socialNetworkProject.service.ProfilePictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ImageFileController {

    @Autowired
    private ProfilePictureService profilePictureService;

    @Autowired
    private ImageFileStorageService imageFileStorageService;

    @Autowired
    private PostService postService;

    @Autowired
    private ImageFileStorageService storageService;

    @GetMapping("/uploadfile")
    public ModelAndView getImageView() {
        return new ModelAndView("uploadfile", "imagefile", new ImageFile());
    }

    @PostMapping("/uploadfile")
    public String uploadImage(@RequestParam("file") MultipartFile file) throws IllegalAccessException {
        imageFileStorageService.storeImageFile(file);
        return "redirect:/gallery";
    }
    @GetMapping("/gallery")
    public String gallery(Model model) throws IllegalAccessException {
        model.addAttribute("allimages", imageFileStorageService.getAllImages());
        return "gallery";
    }

    @PostMapping("/profilepicture")
    public String setProfilePicture(@RequestParam(value = "id", required = false) String pictureId, Model model) throws IllegalAccessException {
        ImageFileDto imageFileDto = imageFileStorageService.getImageFile(pictureId);
        model.addAttribute("newprofilepicture", profilePictureService.setProfilePicture(imageFileDto));
        model.addAttribute("allPosts", postService.getAllPosts());
        return "redirect:/index";
    }
}
