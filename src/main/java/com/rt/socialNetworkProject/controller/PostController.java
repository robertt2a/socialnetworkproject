package com.rt.socialNetworkProject.controller;

import com.rt.socialNetworkProject.model.entity.Post;
import com.rt.socialNetworkProject.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PostController {

    @Autowired
    private PostService postService;


    @GetMapping("/addnewpost")
    public ModelAndView getPostView() {
        return new ModelAndView("addnewpost", "post", new Post());
    }

    @PostMapping("/addnewpost")
    public String addNewPost(Post post) throws IllegalAccessException {
        postService.addPost(post);
        return "redirect:/index";
    }
    @PostMapping("/delete")
    public String deletePost(@RequestParam Long id ){
        postService.deletePost(id);
        return "redirect:/index";
    }

    @PostMapping("/editpost")
    public String editPost(@RequestParam Long id, @RequestParam("newcontent") String content){
        postService.editPost(id, content);
        return "redirect:/index";
    }
}

