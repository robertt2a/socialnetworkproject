package com.rt.socialNetworkProject.controller;

import com.rt.socialNetworkProject.model.entity.Comment;
import com.rt.socialNetworkProject.model.entity.Post;
import com.rt.socialNetworkProject.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class IndexController {

    @Autowired
    private PostService postService;

    @Autowired
    private ProfilePictureService storageService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProfilePictureService profilePictureService;

    @Autowired
    private CommentService commentService;

    @GetMapping("/index")
    public String index(Model model, @ModelAttribute("newpost") Post post, @ModelAttribute("comment") Comment comment) throws IllegalAccessException {
        model.addAttribute("user", userService.findCurrentlyLoggedUser());
        model.addAttribute("allPosts", postService.getAllPosts());
        model.addAttribute("profilepicture", storageService.getProfilePicture());
        model.addAttribute("allprofilepictures",profilePictureService.getAllProfilePictures());
        model.addAttribute("allcoments", commentService.getAllComments());

        return "index";
    }
    @GetMapping("home")
    public String home(){
        return "home";
    }
}
