package com.rt.socialNetworkProject.controller;

import com.rt.socialNetworkProject.exception.LoginAlreadyExistException;
import com.rt.socialNetworkProject.model.dto.UserDto;
import com.rt.socialNetworkProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.text.ParseException;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ExceptionHandler({LoginAlreadyExistException.class})
    public String givenLoginExistInDatabaseError(){
        return "fail";
    }

    @PostMapping("/adduser")
    public ModelAndView addNewUser(@ModelAttribute @Valid UserDto userDto, BindingResult result) {
        userService.addUser(userDto, result);
        return new ModelAndView("login", "user", new UserDto());
    }

    @GetMapping("/adduser")
    public ModelAndView getUserView() {
        return new ModelAndView("adduser", "user", new UserDto());
    }

    @GetMapping("/addadmin")
    public ModelAndView getAdminView() {
        return new ModelAndView("addadmin", "user", new UserDto());
    }

    @PostMapping("/addadmin")
    public String addNewAdmin(@ModelAttribute @Valid UserDto userDto, BindingResult result) {
        userService.addUser(userDto, result);
        return "login";
    }

    @GetMapping("/adminpanel")
    public String adminpanel(Model model) throws IllegalAccessException {
        model.addAttribute("users", userService.getAllUsers());
        return "adminpanel";
    }

    @PostMapping("/bann")
    public String bannChosenUser(@RequestParam("userid") Long id, @RequestParam("date") String date, @RequestParam("time") String time) throws ParseException {
        userService.banUser(id, date, time);
        return "redirect:/adminpanel";
    }
    @PostMapping("/unbann")
    public String unbannChosenUser(@RequestParam("userid")Long id){
        userService.unbanUser(id);
        return "redirect:/adminpanel";
    }
}
