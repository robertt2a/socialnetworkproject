package com.rt.socialNetworkProject.controller;

import com.rt.socialNetworkProject.model.entity.Comment;
import com.rt.socialNetworkProject.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/addnewcomment")
    public ModelAndView getCommentView() {
        return new ModelAndView("addnewcomment", "comment", new Comment());
    }

    @PostMapping("/addnewcomment")
    public String addNewComment(@RequestParam("postidnumber") Long postId, Comment comment) throws IllegalAccessException {
        commentService.addComment(comment, postId);
        return "redirect:/index";
    }
    @PostMapping("/deletecomment")
    public String deletePost(@RequestParam("commentid") Long id ){
        commentService.deleteComment(id);
        return "redirect:/index";
    }

    @PostMapping("/editcomment")
    public String editPost(@RequestParam("commentid") Long id, @RequestParam("commentnewcontent") String content){
        commentService.editComment(id, content);
        return "redirect:/index";
    }
}
