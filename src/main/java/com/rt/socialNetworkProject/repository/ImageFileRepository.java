package com.rt.socialNetworkProject.repository;

import com.rt.socialNetworkProject.model.entity.ImageFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageFileRepository extends JpaRepository<ImageFile, String> {
}
