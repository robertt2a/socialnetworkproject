package com.rt.socialNetworkProject.repository;

import com.rt.socialNetworkProject.model.entity.ProfilePicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilePictureRepository extends JpaRepository<ProfilePicture, Long> {
    ProfilePicture findByUserProfileId(Long id);


}
