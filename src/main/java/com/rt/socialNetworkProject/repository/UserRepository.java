package com.rt.socialNetworkProject.repository;

import com.rt.socialNetworkProject.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    Long countByEmail(String email);
    Optional<User> findByEmail(String email);


}
