package com.rt.socialNetworkProject.service;

import com.rt.socialNetworkProject.model.dto.ImageFileDto;
import com.rt.socialNetworkProject.model.dto.ProfilePictureDto;
import com.rt.socialNetworkProject.model.entity.ProfilePicture;
import com.rt.socialNetworkProject.model.entity.User;
import com.rt.socialNetworkProject.repository.ProfilePictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProfilePictureService {


    @Autowired
    private  UserService userService;

    @Autowired
    private ProfilePictureRepository profilePictureRepository;


    public ProfilePictureDto setProfilePicture(ImageFileDto imageFileDto) throws IllegalAccessException {
        User user = userService.findCurrentlyLoggedUser();
        ProfilePictureDto profilePictureDto = ProfilePictureDto.builder()
                .data(imageFileDto.getData())
                .userProfileId(user.getUserProfile().getId())
                .build();
        if (profilePictureRepository.findByUserProfileId(user.getId())!=null) {
            ProfilePicture picture = profilePictureRepository.findByUserProfileId(user.getId());
            picture.setData(Base64.getDecoder().decode(profilePictureDto.getData()));
            profilePictureRepository.save(picture);
            } else {
                ProfilePicture profilePicture = new ProfilePicture(Base64.getDecoder().decode(profilePictureDto.getData()), user.getUserProfile());
                profilePictureRepository.save(profilePicture);
            }
        return profilePictureDto;
    }

    public ProfilePictureDto getProfilePicture() throws IllegalAccessException {
        User user =userService.findCurrentlyLoggedUser();
        ProfilePicture profilePicture = profilePictureRepository.findByUserProfileId(user.getId());
        if (profilePicture!=null) {
            return ProfilePictureDto.builder()
                    .data(Base64.getEncoder().encodeToString(profilePicture.getData()))
                    .userProfileId(profilePicture.getUserProfile().getId())
                    .build();
        } else {
            return null;
        }
    }

    public Map<Long, ProfilePictureDto> getAllProfilePictures(){
        List<ProfilePicture> profilePictures = profilePictureRepository.findAll();
        Map<Long, ProfilePictureDto> profilePictureDtos = new HashMap<>();
        for (ProfilePicture picture:profilePictures) {
            ProfilePictureDto dto = ProfilePictureDto.builder()
                    .data(Base64.getEncoder().encodeToString(picture.getData()))
                    .userProfileId(picture.getUserProfile().getId())
                    .build();
            profilePictureDtos.put(dto.getUserProfileId(), dto);
        }
        return profilePictureDtos;
    }
}

