package com.rt.socialNetworkProject.service;


import com.rt.socialNetworkProject.model.dto.PostDto;
import com.rt.socialNetworkProject.model.entity.Comment;
import com.rt.socialNetworkProject.model.entity.Post;
import com.rt.socialNetworkProject.model.entity.User;
import com.rt.socialNetworkProject.repository.CommentRepository;
import com.rt.socialNetworkProject.repository.PostRepository;
import com.rt.socialNetworkProject.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostService {

    @Autowired
    private ModelMapper mapper;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;

    public List<PostDto> getAllPosts() {
        List<Post> posts = postRepository.findAll();
        List<PostDto> postDtos = new ArrayList<>();

        for (Post post : posts) {
            PostDto postDto = PostDto.builder()
                    .postId(post.getId())
                    .content(post.getContent())
                    .userProfID(post.getUserProfile().getId())
                    .authorName(post.getUserProfile().getName())
                    .authorSurname(post.getUserProfile().getSurname())
                    .updateDate(post.getUpdatedAt())
                    .build();
            postDtos.add(postDto);
        }
        return postDtos.stream()
                .sorted(Comparator.comparing(PostDto::getUpdateDate, Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList());
    }


    public void addPost(Post post) throws IllegalAccessException {
        User user = userService.findCurrentlyLoggedUser();
        post.setUserProfile(user.getUserProfile());
        postRepository.save(post);
    }

    public void deletePost(Long id) {
        postRepository.deleteById(id);
        List<Comment> commentList = commentRepository.findAll();
        for (Comment comment:commentList) {
            if (comment.getPost().getId().equals(id)){
                commentRepository.delete(comment);
            }
        }

    }

    public void editPost(Long id, String content) {
        Optional<Post> optionalPost = postRepository.findById(id);
        if (optionalPost.isPresent()){
            Post post = optionalPost.get();
            post.setContent(content);
            post.setUpdatedAt(new Date());
            postRepository.save(post);
        }
    }
}