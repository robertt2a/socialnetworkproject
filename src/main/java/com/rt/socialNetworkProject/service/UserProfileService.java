package com.rt.socialNetworkProject.service;

import com.rt.socialNetworkProject.model.dto.UserProfileDto;
import com.rt.socialNetworkProject.model.entity.UserProfile;
import com.rt.socialNetworkProject.repository.UserProfileRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProfileService {
    @Autowired
    private ModelMapper mapper;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserProfileRepository userProfileRepository;

    public List<UserProfileDto> getAllUserProfiles() {
        List<UserProfile> profiles = userProfileRepository.findAll();
        return profiles.stream()
                .map(up -> mapper.map(up, UserProfileDto.class))
                .collect(Collectors.toList());

    }

//    public void addInformationAboutUser(UserProfileDto userProfileDto, BindingResult result) {
//        BindingValidator.validate(result);
//        UserProfile userProfile = mapper.map(userProfileDto, UserProfile.class);
//        userProfile.setAbout(userProfileDto.getAbout());
//        userProfile.setGender(userProfileDto.getGender());
//        userProfile.setLocation(userProfileDto.getLocation());
//        userProfileRepository.save(userProfile);

    }
