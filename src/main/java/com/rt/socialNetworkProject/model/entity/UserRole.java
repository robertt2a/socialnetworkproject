package com.rt.socialNetworkProject.model.entity;

public enum UserRole {
    ROLE_ADMIN, ROLE_USER;
}
