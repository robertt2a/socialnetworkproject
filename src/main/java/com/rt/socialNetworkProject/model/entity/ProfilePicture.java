package com.rt.socialNetworkProject.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "profile_picture")
@Getter
@Setter
@NoArgsConstructor
public class ProfilePicture extends BasicModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private byte[] data;

    @OneToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_profile_id", nullable = false)
    private UserProfile userProfile;

    public ProfilePicture(byte[] data, UserProfile userProfile) {
        this.data = data;
        this.userProfile = userProfile;
    }
}
