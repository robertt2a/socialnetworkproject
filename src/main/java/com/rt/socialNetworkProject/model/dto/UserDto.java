package com.rt.socialNetworkProject.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class UserDto {
    private Long id;
    private String email;
    private String password;
    private String role;
    private UserProfileDto userProfileDto;
    private PostDto postDto;
    private ImageFileDto imageFileDto;
    private boolean enabled;
    private Date banExpireDate;
}
