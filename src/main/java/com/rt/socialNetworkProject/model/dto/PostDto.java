package com.rt.socialNetworkProject.model.dto;

import com.rt.socialNetworkProject.model.entity.Comment;
import lombok.*;

import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostDto{

   private Long postId;
   private String title;
   private String description;
   private String content;
   private String authorName;
   private String authorSurname;
   private Date updateDate;
   private Long userProfID;
   private Comment comment;

}
