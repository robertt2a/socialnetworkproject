package com.rt.socialNetworkProject.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProfileDto {
    private Long id;
    private String about;
    private String gender;
    private String name;
    private String surname;
    private String location;
}
